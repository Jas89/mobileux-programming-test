//Default Type
var Type = "Standard";

//Image Array
var ImageArray = [
    "Images/Nintendo.jpg",
    "Images/MineCraft.jpg",
    "Images/StarCraft.jpg",
    "Images/PacMan.png",
    "Images/Tetris.jpg"
];

$(document).ready(function(){
	if(GetParameters('type')) {
	Type = GetParameters('type');
	}
//On Load
	LoadTiles(8);
    $(".Loader").fadeOut(5000);
    $(".wrapper").fadeOut(5000);

	
//On Add Tile Click
	$(document).on('click','.AddTile', function (e) 
	{
	LoadTiles(1);
	});
	
//On Delete Tile Click
	$(document).on('click','.DeleteTile', function (e) 
	{
	$(this).closest(".GridTile").fadeOut(1500);
	});
	
	
});
//Read Get parameters by name
function GetParameters(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
//Load tiles 
function LoadTiles(NoOfTiles)
	{

	var TileHTML  = "";
		for (i = 0; i < NoOfTiles; i++) 
		{ 
		var MyImage = ImageArray[Math.floor(Math.random()*ImageArray.length)];
		TileHTML  	 += '<div class="col-lg-3 col-md-4 col-xs-12 GridTile">';
		TileHTML 	 += '<a class="thumbnail" href="#">';
		TileHTML 	 += '<img class="img-circle GridTileImage '+Type+'" src="'+MyImage+'" alt="">';
		TileHTML 	 += '<span class="glyphicon glyphicon-minus-sign DeleteTile" aria-hidden="true"></span>';
		TileHTML 	 += '</a>';
		TileHTML 	 += '</div>';
		}
		var MyTileHTML = $(TileHTML).hide();
		$("#GridTiles").prepend(MyTileHTML);
		MyTileHTML.fadeIn(1500);
	}
