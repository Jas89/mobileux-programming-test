# README #

## Instructions ##

## Overview ##
This website is used to add/remove games in the gallery. User can also change the view of the game images to standard or rotating.
default view is set to standard.

## Changing the view ##
1. To Change the view to rotating images, Click three bar icon in the menu(for mobile view)
2. Click Rotating View tab from the menu. 
3. To go back to the standard view Click Standard View.

## Add a game ##
1. Click plus icon next to the image gallery header
2. A random game image will be prepended to the view.

## Remove a game ##
1. Click the minus button on the game image you want to remove.
2. Image will be removed with a fade effect.
